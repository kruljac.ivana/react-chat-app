import React from 'react'
import Topbar from './components/Topbar'
import About from './components/About'
import Chat from './components/Chat'
import SocialSignIn from './components/SocialSignIn'

import { auth } from './firebase'
import { useAuthState } from 'react-firebase-hooks/auth'

const style = {
    wrap: `flex items-center justify-center pt-10 pb-5`,
    h1: `pl-3 text-white text-xl md:text-3xl`,
    appCont: `max-w-[500px] mx-auto text-center`,
    sectionCont: `h-[660px] bg-white rounded-xl shadow-xl mb-6 relative`,
    socialLogin: `socialLogin h-[660px] items-center flex flex-col justify-center rounded-xl overflow-hidden`,
}

function App () {
    const [user] = useAuthState(auth)
    // console.log(user)
    const userClass = user ?  `` : `${style.sectionContLog}`
    return (
        <div className='lg:h-max'>
            <div className={style.wrap}>
                <img src="/logo.png" style={{ height: '60px' }}/>
                <h1 className={style.h1}>&lt;/ Simba Chat App &gt;</h1>
            </div>
            <div className='grid lg:grid-cols-2 p-6 gap-12'>
                <div className='lg:order-first'>
                    <div className={style.appCont}>
                        <section className={style.sectionCont}>
                            {/* Topbar */}
                            {user ? <Topbar /> : ''}
                            {/* Chat component */}
                            {user ? <Chat/> : <div className={style.socialLogin}><SocialSignIn/></div> }
                        </section>
                    </div>
                </div>
                <About />
            </div>
            <div className='pb-10 text-center text-gray-300'>
                <a href="https://ivanakruljac.netlify.app" target="_blank" className='underline'>About</a>
            </div>
        </div>
    )
}

export default App

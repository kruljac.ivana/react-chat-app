import React from 'react'
import { auth } from '../firebase'

const style = {
    msg: `flex item-center shadow-xl m-6 py-2 px-4 rounded-bl-lg rounded-br-lg`,
    name: `absolute mt-[-2rem] text-blue-500 text-xs`,
    left: `left-[2.15rem]`,
    right: `right-[2.15rem]`,
    sent: `bg-[#395dff] text-white text-end float-right rounded-tl-lg`,
    received: `bg-[#e5e5ea] text-black float-left text-start rounded-tr-lg`
}

const Message = ({ message }) => {
    const messageClass = 
        message.uid === auth.currentUser.uid ?  `${style.sent}` : `${style.received}`

    const nameClass = 
        message.uid === auth.currentUser.uid ?  `${style.right}` : `${style.left}`

    return (
        <div>
            <div className={`${style.msg} ${messageClass}`}>
                <p className={`${style.name} ${nameClass}`}>{ message.name ? message.name : 'Anonymous' }</p>
                <p>{ message.text }</p>
            </div>
        </div>
    )
}

export default Message

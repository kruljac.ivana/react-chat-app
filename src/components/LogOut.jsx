import React from 'react'
import { auth } from '../firebase'
import { ReactSVG } from 'react-svg'

const style = {
    logout: `flex items-center bg-transparent underline p-0 hover:text-red-500`
}
const LogOut = () => {
    // const signOut = () => {
    //     signOut(auth)
    // }
    return (
        <>
            <button onClick={() => auth.signOut() } className={style.logout}>Logout
                <ReactSVG className='ml-2' src='/svg/log-out.svg' />
            </button>
        </>
    )
}

export default LogOut

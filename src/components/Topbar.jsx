import React from 'react'
import { auth } from '../firebase'
import { useAuthState } from 'react-firebase-hooks/auth'
import LogOut from './LogOut'

const style = {
    topline: `bg-slate-300 text-sm h-15 mb-3 flex justify-between rounded-t-xl items-center p-4`
}

const Topbar = () => {
    const [user] = useAuthState(auth)
    return (
        <div className={style.topline}>
            {user.displayName ? user.displayName : 'Anonymous' }
            {user ? <LogOut /> : '' }
        </div>
    )
}

export default Topbar

import React from 'react'
import { GoogleLoginButton, createButton } from 'react-social-login-buttons'

import { auth } from '../firebase'
import { GoogleAuthProvider, signInAnonymously, signInWithRedirect } from 'firebase/auth'

const googleSignIn = () => {
    const provider = new GoogleAuthProvider()
    signInWithRedirect(auth, provider)
}

const anonymousSignIn = () => {
    signInAnonymously(auth)
    .then(() => {
      // Signed in..
    })
    .catch((error) => {
      const errorCode = error.code
      const errorMessage = error.message
      // ...
    })
}

const config = {
    text: "Sign in Anonymous",
    icon: "",
    style: { color: "#000000", background: "#ffffff" },
    activeStyle: { background: "#eaeaea" }
}
/* Anonymous login button: */
const AnonymousLoginButton = createButton(config)

const SocialSignIn = () => {
    return (
        <div className='btns w-[250px]'>
            <GoogleLoginButton onClick={ googleSignIn }>Sign in with Goolge</GoogleLoginButton>
            <div className='mb-8'></div>
            <AnonymousLoginButton onClick={ anonymousSignIn } />
        </div>
    )
}

export default SocialSignIn

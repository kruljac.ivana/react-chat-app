import React, { useState, useEffect, useRef } from 'react'
import Message from './Message'
import SendMessage from './SendMessage'
import { db } from '../firebase'
import { query, collection, onSnapshot, orderBy } from 'firebase/firestore'

const style = {
    main: `h-[535px] overflow-y-scroll flex flex-col p-[10px] relative`
}

const Chat = () => {
    const [messages, setMessages] = useState([])
    const scrollTo = useRef()

    useEffect(() => {
        const q = query(collection(db, 'messages'), orderBy('timestamp'))
        const unsubscribe = onSnapshot(q, (querySnapshot) => {
            let messagesArray = []
            querySnapshot.forEach((doc) => {
                messagesArray.push({ ...doc.data(), id: doc.id })
            })
            setMessages(messagesArray)
        })
        return () => unsubscribe()
    }, [])

    return (
        <>
            <main className={style.main}>
                {/* Chat Message Component */}
                { messages && messages.map((msg) => (
                    <Message key={msg.id} message={msg} />
                )) }
                <span ref={scrollTo}></span>
            </main>
            {/* Send Message Component */}
            <SendMessage scroll={scrollTo} />
        </>
    )
}

export default Chat

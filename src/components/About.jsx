import React from 'react'

const About = () => {
    return (
        <div className=' text-white'>
            <div className='text-xl'>Hello webspace wanderer!</div>
            <div className='pt-2'>This is a simple realtime React Chat App.</div>
            <div className='text-xl pt-2'>Tech stack:</div>
            <ul className='list-disc py-4 pl-6'>
                <li>React.js as the main web framework</li>
                <li>Firebase Authentication as the backend service for user authentication (Google login, Anonymous login)</li>
                <li>Firebase Firestore as the realtime backend database</li>
                <li>Tailwind as the CSS framework (v2)</li>
            </ul>
            <div className='pb-2'>Feel free to download code and play around with it:</div>
            <code className='text-blue-500 break-all'>git clone https://gitlab.com/kruljac.ivana/react-chat-app.git</code>
            <img className='hidden lg:block mt-10 border-2 border-slate-500 rounded-xl' src="/react-chat-app.jpg" style={{ width: '550px' }}/>
        </div>
    )
}

export default About

import React, { useState } from 'react'
import { auth, db } from '../firebase'
import { addDoc, collection, serverTimestamp } from 'firebase/firestore'
import { ReactSVG } from 'react-svg'

const style = {
    form: `h-14 w-full max-w-[500px] flex absolute bottom-0`,
    input: `w-full px-5 text-xl bg-slate-200 outline-none rounded-bl-lg border-none`,
    btn: `w-[80px] bg-slate-200 text-white rounded-br-lg font-bold`,
}

const SendMessage = ({ scroll }) => {
    const [iText, setText] = useState('')

    const formSendMessage = async (e) => {
        // disable page reload on form submit:
        e.preventDefault()
        if (iText === '') {
            alert('Please enter a message')
            return
        }
        // destruct to make it easier:
        const { uid, displayName } = auth.currentUser
        // create database:
        await addDoc(collection(db, 'messages'), {
            text: iText,
            name: displayName,
            uid,
            timestamp: serverTimestamp()
        })
        setText('')
        scroll.current.scrollIntoView({behavior: 'smooth'})
    }

    return (
        <form onSubmit={formSendMessage} className={style.form}>
            <input 
                value={iText} 
                onChange={(e) => setText(e.target.value)}
                className={style.input} 
                type="text" 
                placeholder='Type a message ...' 
            />
            <button className={style.btn} type="submit">
                <ReactSVG className='flex justify-center' style={{rotate:'45deg'}} src='/svg/send.svg' />
            </button>
        </form>
    )
}

export default SendMessage
